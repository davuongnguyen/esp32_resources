## Espressif Technology Technical Documentation

C3-Datasheet：https://www.espressif.com.cn/sites/default/files/documentation/esp32-c3_datasheet_en.pdf

C3-Technical Reference Manual：https://www.espressif.com.cn/sites/default/files/documentation/esp32-c3_technical_reference_manual_en.pdf

C3-Hardware Design Guidelines：https://www.espressif.com.cn/sites/default/files/documentation/esp32-c3_hardware_design_guidelines_en.pdf

ESP-IDF:HTTPS://GITHUB.COM/ESPRESSIF/ESP-IDF

ESP32-C3 get-started:
https://docs.espressif.com/projects/esp-idf/en/latest/esp32c3/get-started/

Pin2Pin Development Board:[ESP32-C3-DevKitC-1 &mdash; esp-dev-kits documentation](https://docs.espressif.com/projects/esp-idf/en/latest/esp32c3/hw-reference/esp32c3/user-guide-devkitm-1.html)

[ESP32-C3 Wi-Fi & Bluetooth 5 (LE) MCU | Espressif Systems](https://www.espressif.com/en/products/socs/esp32-C3)

### Link

[Esp32 Esp32-c3 Ai-c3 Esp32-c3-devkitm-1 Esp32-c3-mini-1 Ai-s3 Esp32-s3 N16r8 Esp32-s3-devkit C Dual Type-c Usb Development Board - Integrated Circuits - AliExpress](https://vi.aliexpress.com/i/1005004617322170.html?gatewayAdapt=glo2vnm)
