## Espressif Technology Technical Documentation

S3-Datasheet：https://www.espressif.com.cn/sites/default/files/documentation/esp32-s3_datasheet_en.pdf

S3-Technical Reference Manual：https://www.espressif.com.cn/sites/default/files/documentation/esp32-s3_technical_reference_manual_en.pdf

S3-Hardware Design Guidelines：https://www.espressif.com.cn/sites/default/files/documentation/esp32-S3_hardware_design_guidelines_en.pdf

ESP-IDF:HTTPS://GITHUB.COM/ESPRESSIF/ESP-IDF

ESP32-S3 get-started:
https://docs.espressif.com/projects/esp-idf/en/latest/esp32S3/get-started/

Pin2Pin Development Board:[ESP32-S3-DevKitC-1 &mdash; esp-dev-kits documentation](https://docs.espressif.com/projects/esp-idf/en/latest/esp32s3/hw-reference/esp32s3/user-guide-devkitm-1.html)

[ESP32-S3 Wi-Fi & Bluetooth 5 (LE) MCU | Espressif Systems](https://www.espressif.com/en/products/socs/esp32-S3)

### Link

[Esp32 Esp32-c3 Ai-c3 Esp32-c3-devkitm-1 Esp32-c3-mini-1 Ai-s3 Esp32-s3 N16r8 Esp32-s3-devkit C Dual Type-c Usb Development Board - Integrated Circuits - AliExpress](https://vi.aliexpress.com/i/1005004617322170.html?gatewayAdapt=glo2vnm)
